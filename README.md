# agenda-rest-api

## Use case
![use case diagram](use_case.png)

## Usage

Run `AgendaApplication` from `src/main/java/com/epsi/agenda` repository

## API documentation

Access the API swagger at:
http://localhost:8080/swagger-ui/index.html

Or via:

- Api endpoints details: [open api doc](api_doc/openapi.yaml)

- Http generated requests: [http requests](api_doc/generated-requests.http)
