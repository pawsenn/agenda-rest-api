package com.epsi.agenda.controller;

import com.epsi.agenda.model.Agenda;
import com.epsi.agenda.model.ContactDetail;
import com.epsi.agenda.model.User;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AgendaController.PATH)
@AllArgsConstructor
public class AgendaController {

    public final static String PATH = "/agendas";

    @Operation(summary = "Get all agendas")
    @GetMapping(produces = "application/json")
    public List<Agenda> getAllAgendas() {
        return null;
    }

    @Operation(summary = "Get an agenda by id")
    @GetMapping(path="/{agendaId}", produces="application/json")
    public Agenda getAgendaById(@PathVariable("agendaId") String agendaId) {
        return null;
    }

    @Operation(summary = "Add an agenda")
    @PostMapping(produces = "application/json")
    public User addAgenda(@RequestBody final Agenda agenda) {
        return null;
    }



    @Operation(summary = "Get all users for an agenda")
    @GetMapping(path="/{agendaId}/users", produces="application/json")
    public List<User> getAgendaUsers(@PathVariable("agendaId") String agendaId) {
        return null;
    }

    @Operation(summary = "Get an agenda user by id")
    @GetMapping(path="/{agendaId}/users/{userId}", produces="application/json")
    public User getAgendaUserById(@PathVariable("agendaId") String agendaId, @PathVariable("userId") String userId) {
        return null;
    }

    @Operation(summary = "Add an agenda user")
    @PostMapping(path="/{agendaId}", produces="application/json")
    public User addAgendaUser(@PathVariable("agendaId") String agendaId,
                              @RequestBody final User user) {
        return null;
    }

    @Operation(summary = "Update an agenda user by id")
    @PutMapping(path="/{agendaId}/users/{userId}", produces="application/json")
    public Void updateAgendaUser(@PathVariable("agendaId") String agendaId,
                                 @PathVariable("userId") String userId,
                                 @RequestBody final User user) {
        return null;
    }

    @Operation(summary = "Delete an agenda user by id")
    @DeleteMapping(path="/{agendaId}/users/{userId}", produces="application/json")
    public Void deleteAgendaUser(@PathVariable("agendaId") String agendaId,
                                 @PathVariable("userId") String userId) {
        return null;
    }



    @Operation(summary = "Get all user contact details for an agenda")
    @GetMapping(path="/{agendaId}/users/{userId}/contactdetails/", produces="application/json")
    public ContactDetail getAgendaUserContactDetails(@PathVariable("agendaId") String agendaId,
                                                     @PathVariable("userId") String userId) {
        return null;
    }

    @Operation(summary = "Get an agenda contact detail by id")
    @GetMapping(path="/{agendaId}/users/{userId}/contactdetails/{contactDetailId}", produces="application/json")
    public ContactDetail getAgendaUserContactDetailById(@PathVariable("agendaId") String agendaId,
                                                        @PathVariable("userId") String userId,
                                                        @PathVariable("contactDetailId") String contactDetailId) {
        return null;
    }

    @Operation(summary = "Add an agenda user contact detail")
    @PostMapping(path="/{agendaId}/users/{userId}", produces="application/json")
    public ContactDetail addAgendaUserContactDetail(@PathVariable("agendaId") String agendaId,
                                                    @PathVariable("userId") String userId,
                                                    @RequestBody final ContactDetail contactDetail) {
        return null;
    }

    @Operation(summary = "Update an agenda user contact detail by id")
    @PutMapping(path="/{agendaId}/users/{userId}/contactdetails/{contactDetailId}", produces="application/json")
    public Void updateAgendaUserContactDetail(@PathVariable("agendaId") String agendaId,
                                              @PathVariable("userId") String userId,
                                              @PathVariable("contactDetailId") String contactDetailId,
                                              @RequestBody final ContactDetail contactDetail) {
        return null;
    }

    @Operation(summary = "Delete an agenda user contact detail by id")
    @DeleteMapping(path="/{agendaId}/users/{userId}/contactdetails/{contactDetailId}", produces="application/json")
    public Void deleteAgendaUserContactDetail(@PathVariable("agendaId") String agendaId,
                                              @PathVariable("userId") String userId,
                                              @PathVariable("contactDetailId") String contactDetailId) {
        return null;
    }
}
