package com.epsi.agenda.controller;

import com.epsi.agenda.model.ContactDetail;
import com.epsi.agenda.model.User;
import com.epsi.agenda.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(UserController.PATH)
@AllArgsConstructor
public class UserController {

    public final static String PATH = "/users";

//    FIXME fix endpoint
    @Operation(summary = "Get all users")
    @GetMapping(produces="application/json")
    public List<User> getAllUsers() {
        return null;
    }

    @Operation(summary = "Get a user by id")
    @GetMapping(path="/{userId}", produces="application/json")
    public User getUserById(@PathVariable("userId") String userId) {
        return null;
    }

    @Operation(summary = "Add a user")
    @PostMapping(produces="application/json")
    public User addAgendaUser(@RequestBody final User user) {
        return null;
    }

    @Operation(summary = "Update a user by id")
    @PutMapping(path="/{userId}", produces="application/json")
    public Void updateUser(@PathVariable("userId") String userId, @RequestBody final User user) {
        return null;
    }

    @Operation(summary = "Delete a user by id")
    @DeleteMapping(path="/{userId}", produces="application/json")
    public Void deleteUser(@PathVariable("userId") String userId) {
        return null;
    }



    @Operation(summary = "Get all contact details for a user")
    @GetMapping(path="/{userId}/contactdetails/", produces="application/json")
    public ContactDetail getUserContactDetails(@PathVariable("userId") String userId) {
        return null;
    }

    @Operation(summary = "Get a contact detail by id")
    @GetMapping(path="/{userId}/contactdetails/{contactDetailId}", produces="application/json")
    public ContactDetail getUserContactDetailById(@PathVariable("userId") String userId,
                                                  @PathVariable("contactDetailId") String contactDetailId) {
        return null;
    }

    @Operation(summary = "Add a user contact detail")
    @PostMapping(path="/{userId}", produces="application/json")
    public ContactDetail addUserContactDetail(@PathVariable("userId") String userId,
                                              @RequestBody final ContactDetail contactDetail) {
        return null;
    }

    @Operation(summary = "Update a user contact detail by id")
    @PutMapping(path="/{userId}/contactdetails/{contactDetailId}", produces="application/json")
    public Void updateUserContactDetail(@PathVariable("userId") String userId,
                                        @PathVariable("contactDetailId") String contactDetailId,
                                        @RequestBody final ContactDetail contactDetail) {
        return null;
    }

    @Operation(summary = "Delete a user contact detail by id")
    @DeleteMapping(path="/{userId}/contactdetails/{contactDetailId}", produces="application/json")
    public Void deleteUserContactDetail(@PathVariable("userId") String userId,
                                        @PathVariable("contactDetailId") String contactDetailId) {
        return null;
    }
}
