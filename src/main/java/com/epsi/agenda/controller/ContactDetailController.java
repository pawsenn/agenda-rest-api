package com.epsi.agenda.controller;

import com.epsi.agenda.model.ContactDetail;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ContactDetailController.PATH)
@AllArgsConstructor
public class ContactDetailController {

    public final static String PATH = "/contactdetails";

    @Operation(summary = "Get all contact details")
    @GetMapping(produces="application/json")
    public ContactDetail getAllContactDetails() {
        return null;
    }

    @Operation(summary = "Get a contact detail by id")
    @GetMapping(path="/{contactDetailId}", produces="application/json")
    public ContactDetail getContactDetailById(@PathVariable("contactDetailId") String contactDetailId) {
        return null;
    }

    @Operation(summary = "Add a contact detail")
    @PostMapping(produces="application/json")
    public ContactDetail addContactDetail(@RequestBody final ContactDetail contactDetail) {
        return null;
    }

    @Operation(summary = "Update a contact detail by id")
    @PutMapping(path="/{contactDetailId}", produces="application/json")
    public Void updateContactDetail(@PathVariable("contactDetailId") String contactDetailId,
                                    @RequestBody final ContactDetail contactDetail) {
        return null;
    }

    @Operation(summary = "Delete a contact detail by id")
    @DeleteMapping(path="/{contactDetailId}", produces="application/json")
    public Void deleteContactDetail(@PathVariable("contactDetailId") String contactDetailId) {
        return null;
    }
}
