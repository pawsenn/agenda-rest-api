package com.epsi.agenda.service;

import com.epsi.agenda.model.ContactDetail;
import com.epsi.agenda.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UserService {
    private final Set<ContactDetail> contactDetails = Set.of(
            ContactDetail.builder().build()
    );

    private final List<User> users = List.of(
            new User(1L, "Bob", "Swarovski", contactDetails),
            new User(2L, "Jacques", "Sullivan", contactDetails),
            new User(3L, "Bouh", "Human", contactDetails)
    );

    public List<User> getAllUsers() {
        return users;
    }
}
