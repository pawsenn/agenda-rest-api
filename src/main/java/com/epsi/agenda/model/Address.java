package com.epsi.agenda.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@Builder
public class Address extends ContactDetail {
    private Long id;

    private String address;

    private int postCode;

    private String city;

    private String country;
}
