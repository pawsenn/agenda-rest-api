package com.epsi.agenda.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@Builder
public class Phone extends ContactDetail {
    private Long id;

    private int phoneNumber;
}
