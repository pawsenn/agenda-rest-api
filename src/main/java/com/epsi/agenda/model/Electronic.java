package com.epsi.agenda.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@Builder
public abstract class Electronic extends ContactDetail {
    private Long id;
}
