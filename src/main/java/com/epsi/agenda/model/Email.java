package com.epsi.agenda.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@Builder
public class Email extends Electronic {
    private Long id;

    private String emailAddress;
}
